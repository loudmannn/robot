﻿#define VERBOSE_EXTRA

using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class main_cam : MonoBehaviour {
	public float cameraSensitivity = 90;
	public float climbSpeed = 0.5f;//500;
	public float normalMoveSpeed = 0.5f;//500;
	public float slowMoveFactor = 0.25f;
	public float fastMoveFactor = 3;

	private float rotationX = 0.0f;
	private float rotationY = 0.0f;

	public float camFPS;

	private float lastTime = 0;
	private Camera cam1;
	private Camera cam2;
	private RenderTexture rt;
	private Texture2D tex, tex2;
	private int camX = 320;//1280;
	private int camY = 240;//720;

	private TcpClient server = null;
	private NetworkStream ns = null;
	//private TcpClient server2 = null;
	//private NetworkStream ns2 = null;

	private Vector3 defaultPosition;
	private Quaternion defaultRotation;
	// Use this for initialization
	void Start () {
		//Debug.LogWarning("test");
		//Screen.lockCursor = true;
		//Cursor.visible = false;
		//Cursor.lockState = CursorLockMode.Locked;
		rt = new RenderTexture(camX, camY, 16);
		tex = new Texture2D(camX, camY, TextureFormat.RGB24, false);
		cam1 = GameObject.Find("cam1").GetComponent<Camera>();
		cam1.aspect = (float)camX/(float)camY;
		tex2 = new Texture2D(camX, camY, TextureFormat.RGB24, false);
		cam2 = GameObject.Find("cam2").GetComponent<Camera>();
		cam2.aspect = (float)camX/(float)camY;
		//Vector3 theScale = transform.localScale;
		//theScale.y *= -1;
		//cam1.transform.localScale = theScale;
		//GameObject robot = GameObject.Find("Robot");
		//defaultPosition = robot.transform.position;
		//defaultRotation = robot.transform.rotation;
		//return;
		//Сервер, на который отправляем изображения с камер
		/*try {
			server = new TcpClient("127.0.0.1", 21000);
			ns = server.GetStream();
		} catch (SocketException) {
			Debug.LogError("Unable to connect to server");
			return;
		}*/
	}

	void Update()
	{
		/*rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
		rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
		rotationY = Mathf.Clamp (rotationY, -90, 90);

		transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
		transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

		if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))
		{
			transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
			transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
		}
		else if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl))
		{
			transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
			transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
		}
		else
		{
			transform.position += transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
			transform.position += transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
		}


		if (Input.GetKey (KeyCode.Q)) {transform.position += transform.up * climbSpeed * Time.deltaTime;}
		if (Input.GetKey (KeyCode.E)) {transform.position -= transform.up * climbSpeed * Time.deltaTime;}*/
		if (Input.GetKeyDown (KeyCode.Z)) {
			//Debug.LogWarning("Restart");
			//GameObject robot = GameObject.Find("Robot");
			//robot.transform.position = defaultPosition;
			//robot.transform.rotation = defaultRotation;
			if(ns != null) {
				ns.Close();
				server.Close();
				ns = null;
				server = null;
			}
			try {
				server = new TcpClient("127.0.0.1", 21000);
				ns = server.GetStream();
			} catch (SocketException) {
				Debug.LogError("Unable to connect to server");
				ns = null;
				server = null;
			}
		}

		if (Input.GetKeyDown (KeyCode.V)) {
			if(Cursor.visible == false) {
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			} else {
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
	}

	public void ConnectToServer() {
		if(ns != null) {
			ns.Close();
			server.Close();
			ns = null;
			server = null;
		}
		try {
			server = new TcpClient("127.0.0.1", 21000);
			ns = server.GetStream();
		} catch (SocketException) {
			Debug.LogError("Unable to connect to server");
			ns = null;
			server = null;
		}
	}

	void OnPostRender() {//OnPostRender() {
		//Debug.LogError("OnPostRender");
		if(Time.time > lastTime + .125f/*1/camFPS*/) {
			//Debug.Log("Save");
			lastTime = Time.time;

			cam1.targetTexture = rt;
			RenderTexture.active = rt;
			cam1.Render();
			// Read pixels to texture
			tex.ReadPixels(new Rect(0, 0, camX, camY), 0, 0);
			//byte[] pngImage = tex.EncodeToPNG();
			byte[] rawImage = tex.GetRawTextureData();
			// Read texture to array
			//Color[] framebuffer = tex.GetPixels();
			cam2.targetTexture = rt;
			RenderTexture.active = rt;
			cam2.Render();
			// Read pixels to texture
			tex.ReadPixels(new Rect(0, 0, camX, camY), 0, 0);
			//byte[] pngImage = tex.EncodeToPNG();
			byte[] rawImage2 = tex.GetRawTextureData();

			//System.IO.File.WriteAllBytes(Application.persistentDataPath + "/p.raw", rawImage );
			//Debug.Log("write"+rawImage.Length);
			if(ns != null) {
				try {
					ns.Write(rawImage, 0, rawImage.Length);
					//ns.Flush();
					ns.Write(rawImage2, 0, rawImage2.Length);
					ns.Flush();
				} catch (SocketException) {
					Debug.LogError("Error while send image");
					ns = null;
					server = null;
				}
			}
		}
		RenderTexture.active = null;
	}
}
