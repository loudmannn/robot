﻿using UnityEngine;
using System.Collections;

public class motorScript : MonoBehaviour {

	public double power = 0.0f;
	private float minPower = 0f;
	private float maxPower = 60f;
	private float k;

	void Start () {
		k = 1000f/maxPower;
	}

	void FixedUpdate () {
		float target = (float)(power - 1000)/k + minPower;
		GetComponent<Rigidbody> ().AddRelativeForce (0, target, 0);

	}
}
