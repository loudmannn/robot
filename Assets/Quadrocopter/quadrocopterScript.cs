﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;

public class quadrocopterScript : MonoBehaviour {

	float maxAnlge = 10; //Максимальный угол отклонения pitch, roll
	//фактические параметры
	private double pitch; //Тангаж
	private double roll; //Крен
	private double yaw; //Рыскание
	private bool isStabilizeAlt = false;
	public double throttle; //Газ
	public float alt; //высота
	//требуемые параметры
	public double targetPitch;
	public double targetRoll;
	public double targetYaw;
	public double targetAlt;
	public double altForce; //Изменение тяги для зависания на месте
	//PID регуляторы, которые будут стабилизировать углы
	//каждому углу свой регулятор, класс PID определен ниже
	//константы подобраны на глаз :) пробуйте свои значения
	private PID pitchPID = new PID (1000, 0, 200);
	private PID rollPID = new PID (1000, 0, 200);
	//private PID yawPID = new PID (500, 0, 500);
	private PID altPID = new PID (200, 50, 500);//(700, 0, 700);
	//private Quaternion prevRotation = new Quaternion (0, 1, 0, 0);

	//TCP сервер для приема команд для квадрокоптера
	//Формат: |1 байт: тип элемента(t,p,r,y) | 2 байта: значение(от 1000 до 2000) |
	//throttle: 1400 - 1600: висит на месте
	//pitch, roll: 1000: -15deg, 1500: 0deg, 2000: 15deg
	//yaw: 1000: -90deg/s, 1500: 0 deg/s, 2000: 90 deg/s
	private Socket server;
	private byte[] data = new byte[3];
	private int size = 3;
	private main_cam main_cam;

	void Start() {
		main_cam = GameObject.Find ("Frame").GetComponent<main_cam>();
		server = new Socket(AddressFamily.InterNetwork,
			SocketType.Stream, ProtocolType.Tcp);
		IPEndPoint iep = new IPEndPoint(IPAddress.Any, 21001);
		server.Bind(iep);
		server.Listen(5);
		server.BeginAccept(new AsyncCallback(AcceptConn), server);
	}

	void AcceptConn(IAsyncResult iar)
	{
		Socket oldserver = (Socket)iar.AsyncState;
		Socket client = oldserver.EndAccept(iar);
		Debug.LogError("Client connected: " + client.RemoteEndPoint.ToString());
		//main_cam.ConnectToServer();
		//string stringData = "Welcome to my server";
		//byte[] message1 = Encoding.ASCII.GetBytes(stringData);
		//client.BeginSend(message1, 0, message1.Length, SocketFlags.None,
		//	new AsyncCallback(SendData), client);
		client.BeginReceive(data, 0, size, SocketFlags.None,
				new AsyncCallback(ReceiveData), client);
	}

	/*void SendData(IAsyncResult iar)
	{
		Socket client = (Socket)iar.AsyncState;
		int sent = client.EndSend(iar);
		client.BeginReceive(data, 0, size, SocketFlags.None,
			new AsyncCallback(ReceiveData), client);
	}*/

	void ReceiveData(IAsyncResult iar)
	{
		Socket client = (Socket)iar.AsyncState;
		int recv = client.EndReceive(iar);
		//Debug.LogError("ReceiveData: " + recv);
		if (recv == 0)
		{
			client.Close();
			Debug.LogError("Client disconnected, waiting...");
			server.BeginAccept(new AsyncCallback(AcceptConn), server);
			return;
		} else if(recv > 3) {
			Debug.LogError("Error format: " + data);
		}
		int value = (data[1]<<8) + data[2];
		switch(data[0]) {
		case (byte)'t': throttle = value; break;
		case (byte)'p':
			value -= 1500;//-500..500
			targetPitch = value / (500/maxAnlge);//-maxAngle..maxAngle
			break;
		case (byte)'r':
			value -= 1500;//-500..500
			targetRoll = value / (500/maxAnlge);//-maxAngle..maxAngle
			break;
		case (byte)'y':
			//Debug.Log("yaw value: " + value);
			value -= 1500;//-500..500
			targetYaw = value / (500.0f/90.0f);//-90..90
			//if(targetYaw < 4 && targetYaw > -4) {
			//	targetYaw = 0;
			//}
			//Debug.Log("yaw targetYaw: " + targetYaw);
			break;
		default:
			Debug.LogError("Unknown type: " + data[0]);
			break;
		}
		client.BeginReceive(data, 0, size, SocketFlags.None,
			new AsyncCallback(ReceiveData), client);

		//string receivedData = Encoding.ASCII.GetString(data, 0, recv);
		//results.Items.Add(receivedData);
		//byte[] message2 = Encoding.ASCII.GetBytes(receivedData);
		//client.BeginSend(message2, 0, message2.Length, SocketFlags.None,
		//	new AsyncCallback(SendData), client);
	}

	void readRotation () {
		
		//фактическая ориентация нашего квадрокоптера,
		//в реальном квадрокоптере эти данные необходимо получать
		//из акселерометра-гироскопа-магнетометра, так же как делает это ваш
		//смартфон
		Vector3 rot = GameObject.Find ("Frame").GetComponent<Transform> ().rotation.eulerAngles;
		pitch = rot.x;
		yaw = rot.y;
		roll = rot.z;

	}
	void readAlt () {

		//фактическая ориентация нашего квадрокоптера,
		//в реальном квадрокоптере эти данные необходимо получать
		//из акселерометра-гироскопа-магнетометра, так же как делает это ваш
		//смартфон
		alt = GameObject.Find ("Frame").GetComponent<Transform> ().position.y;

	}

	//функция стабилизации квадрокоптера
	//с помощью PID регуляторов мы настраиваем
	//мощность наших моторов так, чтобы углы приняли нужные нам значения
	void stabilize () {

		//нам необходимо посчитать разность между требуемым углом и текущим
		//эта разность должна лежать в промежутке [-180, 180] чтобы обеспечить
		//правильную работу PID регуляторов, так как нет смысла поворачивать на 350
		//градусов, когда можно повернуть на -10

		double dPitch = targetPitch - pitch;
		double dRoll = targetRoll - roll;
		//double dYaw = targetYaw - yaw;

		dPitch -= Math.Ceiling (Math.Floor (dPitch / 180.0) / 2.0) * 360.0;
		dRoll -= Math.Ceiling (Math.Floor (dRoll / 180.0) / 2.0) * 360.0;
		//dYaw -= Math.Ceiling (Math.Floor (dYaw / 180.0) / 2.0) * 360.0;

		//1 и 2 мотор впереди
		//3 и 4 моторы сзади
		double throttle1;
		if((throttle >= 1400) && (throttle <= 1600)) {
			throttle1 = 1500;
		}
		else if(throttle > 1600) {
			throttle1 = throttle - 180;
		} else {
			throttle1 = throttle;
		}
		double motor1power = throttle1;
		double motor2power = throttle1;
		double motor3power = throttle1;
		double motor4power = throttle1;

		//ограничитель на мощность подаваемую на моторы,
		//чтобы в сумме мощность всех моторов оставалась
		//одинаковой при регулировке
		double powerLimit = throttle1 > 1400 ? 1400 : throttle1;

		//управление тангажем:
		//на передние двигатели подаем возмущение от регулятора
		//на задние противоположное возмущение
		double pitchForce = - pitchPID.calc (0, dPitch / 180.0);
		pitchForce = pitchForce > powerLimit ? powerLimit : pitchForce;
		pitchForce = pitchForce < -powerLimit ? -powerLimit : pitchForce;
		motor1power +=   pitchForce;
		motor2power +=   pitchForce;
		motor3power += - pitchForce;
		motor4power += - pitchForce;

		//управление креном:
		//действуем по аналогии с тангажем, только регулируем боковые двигатели
		double rollForce = - rollPID.calc (0, dRoll / 180.0);
		rollForce = rollForce > powerLimit ? powerLimit : rollForce;
		rollForce = rollForce < -powerLimit ? -powerLimit : rollForce;
		motor1power +=   rollForce;
		motor2power += - rollForce;
		motor3power += - rollForce;
		motor4power +=   rollForce;

		//управление рысканием:
		/*double yawForce = yawPID.calc (0, dYaw / 180.0);
		yawForce = yawForce > powerLimit ? powerLimit : yawForce;
		yawForce = yawForce < -powerLimit ? -powerLimit : yawForce;
		motor1power +=   yawForce;
		motor2power += - yawForce;
		motor3power +=   yawForce;
		motor4power += - yawForce;*/

		double yawForce = 5f;
		motor1power +=   yawForce*targetYaw;
		motor2power += - yawForce*targetYaw;
		motor3power +=   yawForce*targetYaw;
		motor4power += - yawForce*targetYaw;

		if (isStabilizeAlt) {
			readAlt ();
			altForce = altPID.calc (alt, targetAlt);
			motor1power +=   altForce;
			motor2power +=   altForce;
			motor3power +=   altForce;
			motor4power +=   altForce;
		}

		GameObject.Find ("Motor1").GetComponent<motorScript>().power = motor1power;
		GameObject.Find ("Motor2").GetComponent<motorScript>().power = motor2power;
		GameObject.Find ("Motor3").GetComponent<motorScript>().power = motor3power;
		GameObject.Find ("Motor4").GetComponent<motorScript>().power = motor4power;
	}

	/*void stabilizeAlt() {
		double motor1power = throttle;
		double motor2power = throttle;
		double motor3power = throttle;
		double motor4power = throttle;
		readAlt ();
		 altForce = altPID.calc (alt, targetAlt);
		motor1power +=   altForce;
		motor2power +=   altForce;
		motor3power +=   altForce;
		motor4power +=   altForce;

		GameObject.Find ("Motor1").GetComponent<motorScript>().power = motor1power;
		GameObject.Find ("Motor2").GetComponent<motorScript>().power = motor2power;
		GameObject.Find ("Motor3").GetComponent<motorScript>().power = motor3power;
		GameObject.Find ("Motor4").GetComponent<motorScript>().power = motor4power;
	}*/

	void Update() {
		float deltaThrottle = 10f;
		if (Input.GetKey (KeyCode.U)) { throttle += deltaThrottle;}
		if (Input.GetKey (KeyCode.O)) { throttle -= deltaThrottle;}
		if (Input.GetKeyDown (KeyCode.I)) { targetPitch = maxAnlge;}
		if (Input.GetKeyDown (KeyCode.J)) { targetRoll = maxAnlge;}
		if (Input.GetKeyDown (KeyCode.K)) { targetPitch = -maxAnlge;}
		if (Input.GetKeyDown (KeyCode.L)) { targetRoll = -maxAnlge;}
		if (Input.GetKeyUp (KeyCode.I)) { targetPitch = 0;}
		if (Input.GetKeyUp (KeyCode.J)) { targetRoll = 0;}
		if (Input.GetKeyUp (KeyCode.K)) { targetPitch = -0;}
		if (Input.GetKeyUp (KeyCode.L)) { targetRoll = -0;}
		if (Input.GetKeyDown (KeyCode.Y)) { targetYaw = -90; }
		if (Input.GetKeyDown (KeyCode.T)) { targetYaw = 90; }
		if (Input.GetKeyUp (KeyCode.Y) || Input.GetKeyUp (KeyCode.T)) { targetYaw = 0; }
	}

	//как советуют в доке по Unity вычисления проводим в FixedUpdate, а не в Update
	void FixedUpdate () {
		float deltaYaw = 1;
		//targetPitch = 0;
		//targetRoll = 0;
		if(throttle < 1000) {throttle = 1000;}
		if(throttle > 2000) {throttle = 2000;}
		if (throttle < 1600 && throttle > 1400) {
			isStabilizeAlt = true;
		} else {
			readAlt ();
			targetAlt = alt;
			isStabilizeAlt = false;
		}

		/*if (Input.GetKey (KeyCode.Y)) {
			targetYaw += deltaYaw;
			if(targetYaw > 360) {targetYaw = 0;}
		}
		if (Input.GetKey (KeyCode.T)) {
			targetYaw -= deltaYaw;
			if(targetYaw < 0) {targetYaw = 360;}
		}*/

		readRotation ();
		stabilize ();
	}
	
}

public class PID {
	
	private double P;
	private double I;
	private double D;
	
	private double prevErr;
	private double sumErr;
	
	public PID (double P, double I, double D) {
		this.P = P;
		this.I = I;
		this.D = D;
	}
	
	public double calc (double current, double target) {
		
		double dt = Time.fixedDeltaTime;
		
		double err = target - current;
		this.sumErr += err;
		
		double force = this.P * err + this.I * this.sumErr * dt + this.D * (err - this.prevErr) / dt;
		
		this.prevErr = err;
		return force;
	}
	
};
