#python3
# -*- coding: utf-8 -*-
#from __future__ import division
 
import cv2
import numpy as np
#import imutils
import random
import math
import time
import datetime
import socket
#import serial
import threading
import enum
 
##############################
# Константы
# Разрешение камеры
X = 320#800
Y = 240#600
# Ширина полоски
#STRIP_SIZE = 20
# Цвет линии в HSV (H: 0 - 180, S: 0 - 255, V: 0 - 255)
#black
BLACK_MIN = np.array([0, 0, 0])
BLACK_MAX = np.array([180, int(30/100*255), int(60/100*255)])
#orange
ORANGE_MIN = np.array([0, int(10/100*255), int(40/100*255)])
ORANGE_MAX = np.array([30, int(100/100*255), int(100/100*255)])
#red
RED_MIN = np.array([300//2, int(10/100*255), int(25/100*255)])
RED_MAX = np.array([360//2, 255, 255])
MIN_ANGLE = 0.4
MIN_ROLL_ERROR = 2
MAX_ROLL_PITCH_ANGLE = 10 # Макс. возможный угол наклона
MAX_TARGET_ROLL_PITCH_ANGLE = 3 # Ограничение макс. угла
MAX_ANGLE_COEFF = MAX_TARGET_ROLL_PITCH_ANGLE/MAX_ROLL_PITCH_ANGLE
#FORWARD_SLEEP_TIME = 0.5
#FORWARD_SLEEP_TIME_END = 0.1
#ROTATE_SLEEP_TIME = 0.1
#ROTATE_SLEEP_TIME_END = 0.1

IS_SIMULATION = True
IS_CAM_TEST = False
#serverIP = '10.0.0.1'
#serverPort = 21000
##############################

class PortFake():
    def write(self, d):
        pass

class TCP_Client():
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.settimeout(5)

        try:
            self.sock.connect(("127.0.0.1", 21001))
        except socket.timeout as e:
            print('Не удалось подключиться к серверу')
        print('Connected to 21001')

    def write(self, msg):
        try:
            self.sock.sendall(msg)
        except socket.timeout as e:
            print('Сервер не ответил')

def toRad(deg):
    return deg * math.pi/180

def toDeg(rad):
    return 180*rad / math.pi

def angleBetweenVectors(a,b):
    c = np.dot(a,b)/np.linalg.norm(a)/np.linalg.norm(b)
    angleRad = np.arccos(np.clip(c, -1, 1))
    return toDeg(angleRad)

#def angleBetweenVectors(a,b):
#    cs = (a[0]*b[0] + a[1]*b[1]) / (1 * math.sqrt(b[0]*b[0] + b[1]*b[1]))
#    return toDeg(math.acos(np.clip(cs, -1, 1)))

class PID():
    def __init__(self, P, I, D):
        self.P = P
        self.I = I
        self.D = D
        self.prevErr = 0
        self.sumErr = 0

    def calc (self, current, target):
        dt = 0.125#Time.fixedDeltaTime;

        err = target - current
        self.sumErr += err

        force = self.P * err + self.I * self.sumErr * dt + self.D * (err - self.prevErr) / dt
        self.prevErr = err
        return force

    def reset (self):
        self.prevErr = 0
        self.sumErr = 0

# Начальное состояние
class StartState():
    def __init__(self):
        pass
    def start(self, robot, nextState):
        self.robot = robot
        self.nextState = nextState
    def exit(self):
        pass
    def action(self, imgs):
        self.robot.state = self.nextState
        cv2.imshow('gray', imgs[0])
        cv2.imshow('grayFront', imgs[1])

# Конечное состояние
# Просто выводит картинку с камер
class EndState():
    def __init__(self):
        pass
    def start(self, robot):
        self.robot = robot
    def exit(self):
        pass
    def action(self, imgs):
        img_hsv = cv2.cvtColor(imgs[0], cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
        img_hsv = cv2.cvtColor(imgs[1], cv2.COLOR_BGR2HSV)
        img_gray_front = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
        cv2.imshow('gray', img_gray)
        cv2.imshow('grayFront', img_gray_front)

# Движение от старта до ворот
class MoveToGate():
    def __init__(self):
        pass
    def start(self, robot):
        #self.pid = PID(1,0,0)
        self.robot = robot
    def exit(self):
        self.robot.notMove()
    def action(self, imgs):
        img = imgs[1]
        #img_blur = cv2.medianBlur(imgs[1], 21)
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, RED_MIN, RED_MAX)
        img_gray_nonzero = img_gray.nonzero()
        if len(img_gray_nonzero[1])>22000:
            self.robot.state = RobotState.MOVE_NEAR_GATE
            #print("->MOVE_NEAR_GATE")
        elif len(img_gray_nonzero[1])>0:
            x_mean = img_gray_nonzero[1].mean()
            xOffset = X//2 - x_mean
            print("Roll error", xOffset, end='; ')
            if abs(xOffset) < MIN_ROLL_ERROR:
                print("Go", end='; ')
                self.robot.notRoll()
                self.robot.pitch(Y//2)
            else:
                xOffsetDt = self.robot.rollPID.calc(0, xOffset)
                self.robot.notPitch()
                self.robot.roll(xOffsetDt)
            #print(len(img_gray_nonzero[1]), end=';')
            #print('Mean X', x_mean, end=';')
            print("")
        cv2.imshow('grayFront', img_gray)

# Движение до нижней перекладины ворот
class MoveNearGate():
    def __init__(self):
        pass
    def start(self, robot):
        self.robot = robot
    def exit(self):
        self.robot.notMove()
    def action(self, imgs):
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, RED_MIN, RED_MAX)
        self.robot.pitch(Y//2)
        line_posY = Y//2
        img_gray_nonzero = img_gray[line_posY].nonzero()
        if (len(img_gray_nonzero[0]) > 4):
            self.robot.state = RobotState.MOVE_NEAR_GATE2
            #print('->MOVE_NEAR_GATE2')
        cv2.imshow('gray', img_gray)

# Движение от нижней перекладины на расстояние,
# на котором она не будет видна
class MoveNearGate2():
    def __init__(self):
        pass
    def start(self, robot):
        self.robot = robot
    def exit(self):
        self.robot.notMove()
    def action(self, imgs):
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, RED_MIN, RED_MAX)
        line_posX1 = X//3
        line_posX2 = 2*X//3
        img_gray_nonzero1 = img_gray[0:Y, line_posX1:line_posX1+1].nonzero()
        img_gray_nonzero2 = img_gray[0:Y, line_posX2:line_posX2+1].nonzero()
        if(len(img_gray_nonzero1[0])>0 and len(img_gray_nonzero2[0])>0):
            ym1 = int(img_gray_nonzero1[0].mean())
            ym2 = int(img_gray_nonzero2[0].mean())
            a = (1, 0)
            b = (line_posX1, ym2-ym1)
            #print(b)
            cv2.circle(img_gray, (line_posX1, ym1), 5, (0,0,255))
            cv2.circle(img_gray, (line_posX1, ym1), 8, (255,255,255))
            cv2.circle(img_gray, (line_posX2, ym2), 5, (0,0,255))
            cv2.circle(img_gray, (line_posX2, ym2), 8, (255,255,255))
            rotateAngle = angleBetweenVectors(a, b)
            if b[1] < 0:
                rotateAngle = -rotateAngle
            print("Yaw error", rotateAngle, end='; ')
            if abs(rotateAngle) < MIN_ANGLE:
                print("Go", end='; ')
                self.robot.notRotate()
                self.robot.notRoll()
                self.robot.pitch(Y//2)
                #time.sleep(1)
                #self.notPitch()
                #self.state = RobotState.ON_LINE
            else:
                rotateAngleDt = self.robot.rotatePID.calc(0, rotateAngle)
                self.robot.notRoll()
                self.robot.notPitch()
                self.robot.rotate(rotateAngleDt)
            print('')
        else:
            self.robot.state = RobotState.MOVE_ON_LINE1
        cv2.imshow('gray', img_gray)

# Движение после ворот до линии 1,
# выравнивание по центру линии и поворот до угла 0
class MoveOnLine1():
    def __init__(self):
        pass
    def start(self, robot):
        self.robot = robot
        self.isDownReached = False
    def exit(self):
        self.robot.notMove()
        #self.robot.rollPID.reset()
        #self.robot.rotatePID.reset()
    def fixRotateError(self, img_gray):
        # y1 = Y//2-20
        # x1 = X//2-20
        # y2 = Y//2-20
        # x2 = X//2-20
        # y3 = Y//2-20
        # x3 = X//2+19
        # i1 = img_gray[y1:Y//2-19, x1:X//2+20].nonzero()
        # i2 = img_gray[y2:Y//2, x2: X//2-19].nonzero()
        # i3 = img_gray[y3:Y//2, x3: X//2+20].nonzero()
        upSpace = img_gray[0:Y//2, 0:X].nonzero()
        downSpace = img_gray[Y//2:Y, 0:X].nonzero()
        #if(len(i1[0])>0 or len(i2[0])>0 or len(i3[0])>0):
            # count = 0
            # if(len(i1[0])>0):
            #     ym1 = i1[0].mean() + y1
            #     xm1 = i1[1].mean() + x1
            #     count += 1
            # else:
            #     ym1 = xm1 = 0
            # if(len(i2[0])>0):
            #     ym2 = i2[0].mean() + y2
            #     xm2 = i2[1].mean() + x2
            #     count += 1
            # else:
            #     ym2 = xm2 = 0
            # if(len(i3[0])>0):
            #     ym3 = i3[0].mean() + y3
            #     xm3 = i3[1].mean() + x3
            #     count += 1
            # else:
            #     ym3 = xm3 = 0
            # xm = int((xm1 + xm2 + xm3) // count)
            # ym = int((ym1 + ym2 + ym3) // count)
        if( len(upSpace[0])>0 and len(downSpace[0])>0):
            ym1 = upSpace[0].mean()
            xm1 = upSpace[1].mean()
            ym2 = downSpace[0].mean() + Y//2
            xm2 = downSpace[1].mean()
            a = (0, Y)
            b = (xm1 - xm2, Y - (ym1 + (Y - ym2)))
            #b = (xm - X//2, Y - ym)
            rotateAngle = angleBetweenVectors(a, b)
            if b[0] < 0:
                rotateAngle = -rotateAngle
            print("Yaw error", rotateAngle, end='; ')
            if abs(rotateAngle) < MIN_ANGLE:
                print("<MIN_ANGLE", end='; ')
                self.robot.notRotate()
                line_center = img_gray[Y//2:Y//2+1, 0:X].nonzero()
                if len(line_center[0])>=89 and (not self.isDownReached):
                    print("\nDown reached\n", end='; ')
                    self.isDownReached = True
                if not self.isDownReached:
                    self.robot.throttle(1350)
                else:
                    self.robot.throttle(1620)
                img_hsv_front = cv2.cvtColor(self.imgs[1], cv2.COLOR_BGR2HSV)
                img_gray_front = cv2.inRange(img_hsv_front, RED_MIN, RED_MAX)
                i4 = img_gray_front.nonzero()
                if len(i4[0])>0:
                    ym4 = i4[0].mean()
                    xm4 = i4[1].mean()
                    if (ym4 > Y//2+30-5) and (ym4 < Y//2+30+5):
                        print("Ball detected", end='; ')
                        self.robot.state = RobotState.MOVE_TO_LINE2
                        #self.robot.throttle(1500)
                    else:
                        print("ym4", ym4, end='; ')
                cv2.imshow('grayFront', img_gray_front)
                #self.robot.state = RobotState.END
                #print("Go", rotateAngle, end=';')
                #self.notRotate()
                #self.pitch(Y//2)
                #time.sleep(1)
                #self.notPitch()
                #self.state = RobotState.ON_LINE
            else:
#                pass
                rotateAngleDt = self.robot.rotatePID.calc(0, rotateAngle)
                #self.notRoll()
                #self.notPitch()
                self.robot.throttle(1500)
                self.robot.rotate(rotateAngleDt)
            cv2.circle(img_gray, (int(xm1), int(ym1)), 5, (0,0,255))
            cv2.circle(img_gray, (int(xm1), int(ym1)), 8, (255,255,255))
            cv2.circle(img_gray, (int(xm2), int(ym2)), 5, (0,0,255))
            cv2.circle(img_gray, (int(xm2), int(ym2)), 8, (255,255,255))
    def fixPitchError(self, img_gray, img_gray_nonzero):
        y_mean = int(img_gray_nonzero[0].mean())
        yOffset = Y//2 - y_mean
        print("Pitch error", yOffset, end='; ')
        if abs(yOffset) < MIN_ROLL_ERROR:
            self.robot.notPitch()
            self.fixRotateError(img_gray)
        else:
            yOffsetDt = self.robot.pitchPID.calc(0, yOffset)
            self.robot.notRoll()
            self.robot.notRotate()
            self.robot.throttle(1500)
            self.robot.pitch(yOffsetDt)
    def fixRollError(self, img_gray, img_gray_nonzero):
        x_mean = int(img_gray_nonzero[1].mean())
        xOffset = X//2 - x_mean
        print("Roll error", xOffset, end='; ')
        if abs(xOffset) < MIN_ROLL_ERROR:
            self.robot.notRoll()
            self.fixPitchError(img_gray, img_gray_nonzero)
        else:
            xOffsetDt = self.robot.rollPID.calc(0, xOffset)
            self.robot.notPitch()
            self.robot.notRotate()
            self.robot.throttle(1500)
            self.robot.roll(xOffsetDt)
    def action(self, imgs):
        self.imgs = imgs
        img = imgs[0]
        #img_blur = cv2.medianBlur(imgs[1], 21)
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
        img_gray_nonzero = img_gray.nonzero()
        if len(img_gray_nonzero[0])>0:
            self.fixRollError(img_gray, img_gray_nonzero)
            #if abs(xOffset) < MIN_ROLL_ERROR:
            #    print("Go", end='; ')
            #    self.robot.notRoll()
            #    self.robot.pitch(Y//2)
            #else:
            #    xOffsetDt = self.robot.rollPID.calc(0, xOffset)
            #    self.robot.notPitch()
            #    self.robot.roll(xOffsetDt)
            #print(len(img_gray_nonzero[1]), end=';')
            #print('Mean X', x_mean, end=';')
            print("")
        cv2.line(img_gray, (X//2, 0), (X//2, Y), (0,0,255))
        cv2.line(img_gray, (X//2+1, 0), (X//2+1, Y), (255,255,255))
        cv2.line(img_gray, (0, Y//2), (X, Y//2), (0,0,255))
        cv2.line(img_gray, (0, Y//2+1), (X, Y//2+1), (255,255,255))
        cv2.imshow('gray', img_gray)

# Движение по линии 2 и 3,
# выравнивание по центру линии и поворот до угла 0
class MoveOnLine23():
    def __init__(self, line_num):
        self.line_num = line_num
    def start(self, robot):
        self.robot = robot
        self.isDownReached = False
    def exit(self):
        self.robot.notMove()
    def fixRotateError(self, img_gray):
        upSpace = img_gray[0:Y//2, 0:X].nonzero()
        downSpace = img_gray[Y//2:Y, 0:X].nonzero()
        if( len(upSpace[0])>0 and len(downSpace[0])>0):
            ym1 = upSpace[0].mean()
            xm1 = upSpace[1].mean()
            ym2 = downSpace[0].mean() + Y//2
            xm2 = downSpace[1].mean()
            a = (0, Y)
            b = (xm1 - xm2, Y - (ym1 + (Y - ym2)))
            rotateAngle = angleBetweenVectors(a, b)
            if b[0] < 0:
                rotateAngle = -rotateAngle
            print("Yaw error", rotateAngle, end='; ')
            if abs(rotateAngle) < MIN_ANGLE:
                self.robot.notRotate()
                if self.line_num == 2:
                    self.robot.state = RobotState.MOVE_TO_SHAPE
                elif self.line_num == 3:
                    self.robot.pitch(Y//2)
                    time.sleep(9)
                    self.robot.notPitch()
                    self.robot.throttle(1800)
                    time.sleep(2)
                    self.robot.throttle(1500)
                    self.robot.state = RobotState.END
            else:
                rotateAngleDt = self.robot.rotatePID.calc(0, rotateAngle)
                self.robot.rotate(rotateAngleDt)
            cv2.circle(img_gray, (int(xm1), int(ym1)), 5, (0,0,255))
            cv2.circle(img_gray, (int(xm1), int(ym1)), 8, (255,255,255))
            cv2.circle(img_gray, (int(xm2), int(ym2)), 5, (0,0,255))
            cv2.circle(img_gray, (int(xm2), int(ym2)), 8, (255,255,255))
    def fixPitchError(self, img_gray, img_gray_nonzero):
        y_mean = int(img_gray_nonzero[0].mean())
        yOffset = Y//2 - y_mean
        print("Pitch error", yOffset, end='; ')
        if abs(yOffset) < MIN_ROLL_ERROR:
            self.robot.notPitch()
            self.fixRotateError(img_gray)
        else:
            yOffsetDt = self.robot.pitchPID.calc(0, yOffset)
            self.robot.notRoll()
            self.robot.notRotate()
            self.robot.throttle(1500)
            self.robot.pitch(yOffsetDt)
    def fixRollError(self, img_gray, img_gray_nonzero):
        x_mean = int(img_gray_nonzero[1].mean())
        xOffset = X//2 - x_mean
        print("Roll error", xOffset, end='; ')
        if abs(xOffset) < MIN_ROLL_ERROR:
            self.robot.notRoll()
            self.fixPitchError(img_gray, img_gray_nonzero)
        else:
            xOffsetDt = self.robot.rollPID.calc(0, xOffset)
            self.robot.notPitch()
            self.robot.notRotate()
            self.robot.throttle(1500)
            self.robot.roll(xOffsetDt)
    def action(self, imgs):
        self.imgs = imgs
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
        img_gray_nonzero = img_gray.nonzero()
        if len(img_gray_nonzero[0])>0:
            self.fixRollError(img_gray, img_gray_nonzero)
            print("")
        cv2.line(img_gray, (X//2, 0), (X//2, Y), (0,0,255))
        cv2.line(img_gray, (X//2+1, 0), (X//2+1, Y), (255,255,255))
        cv2.line(img_gray, (0, Y//2), (X, Y//2), (0,0,255))
        cv2.line(img_gray, (0, Y//2+1), (X, Y//2+1), (255,255,255))
        cv2.imshow('gray', img_gray)

# Движение до линии 2 или 3,
# поворот в сторону линии
class MoveToLine23():
    def __init__(self, line_num):
        self.line_num = line_num
    def start(self, robot):
        self.robot = robot
        self.isNextLineFound = False
        self.robot.pitch(Y//2)
        # задержка только для движения с линии 1
        if self.line_num == 2:
            time.sleep(8)
    def exit(self):
        self.robot.notMove()
    def action(self, imgs):
        self.imgs = imgs
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
        #line_posY = 2*Y//3
        img_gray_nonzero = img_gray[2*Y//3].nonzero()
        #img_gray_nonzero = img_gray[Y//2+20: Y//2+21, X//2-20: X//2+20].nonzero()
        if (len(img_gray_nonzero[0]) > 4 or self.isNextLineFound): # > 4 пикселя на линии
            self.isNextLineFound = True
            img_gray_nonzero = img_gray.nonzero()
            if( len(img_gray_nonzero[0])>0 ):
                ym = img_gray_nonzero[0].mean()
                xm = img_gray_nonzero[1].mean()
                a = (0, 1)
                b = (xm-X//2, ym+Y//2)
                cv2.circle(img_gray, (int(xm), int(ym)), 5, (0,0,255))
                cv2.circle(img_gray, (int(xm), int(ym)), 8, (255,255,255))
                rotateAngle = angleBetweenVectors(a, b)
                if b[0] < 0:
                    rotateAngle = -rotateAngle
                print("Yaw error", rotateAngle, end='; ')
                if abs(rotateAngle) < MIN_ANGLE:
                    self.robot.notRotate()
                    if self.line_num == 2:
                        self.robot.state = RobotState.MOVE_ON_LINE2
                    elif self.line_num == 3:
                        self.robot.state = RobotState.MOVE_ON_LINE3
                else:
                    rotateAngleDt = self.robot.rotatePID.calc(0, rotateAngle)
                    self.robot.notRoll()
                    self.robot.notPitch()
                    self.robot.rotate(rotateAngleDt)
                print("")
        else:
            self.robot.pitch(Y//2)
        cv2.line(img_gray, (0, 2*Y//3), (X, 2*Y//3), (0,0,255))
        cv2.line(img_gray, (0, 2*Y//3+1), (X, 2*Y//3+1), (255,255,255))
        cv2.line(img_gray, (X//2, 0), (X//2, Y), (0,0,255))
        cv2.line(img_gray, (X//2+1, 0), (X//2+1, Y), (255,255,255))
        cv2.line(img_gray, (0, Y//2), (X, Y//2), (0,0,255))
        cv2.line(img_gray, (0, Y//2+1), (X, Y//2+1), (255,255,255))
        cv2.imshow('gray', img_gray)


# Движение до фигуры,
# выравнивание по центру
class MoveToShape():
    def __init__(self):
        pass
    def start(self, robot):
        self.robot = robot
        self.isShapeFound = False
        self.robot.pitch(Y//2)
    def exit(self):
        self.robot.notMove()
    def fixPitchError(self, img_gray, img_gray_nonzero):
        y_mean = int(img_gray_nonzero[0].mean())
        yOffset = Y//2 - y_mean
        print("Pitch error", yOffset, end='; ')
        if abs(yOffset) < MIN_ROLL_ERROR:
            self.robot.notPitch()
            self.robot.state = RobotState.MOVE_ON_SHAPE
        else:
            yOffsetDt = self.robot.pitchPID.calc(0, yOffset)
            self.robot.notRoll()
            self.robot.notRotate()
            self.robot.pitch(yOffsetDt)
    def fixRollError(self, img_gray, img_gray_nonzero):
        x_mean = int(img_gray_nonzero[1].mean())
        xOffset = X//2 - x_mean
        print("Roll error", xOffset, end='; ')
        if abs(xOffset) < MIN_ROLL_ERROR:
            self.robot.notRoll()
            self.fixPitchError(img_gray, img_gray_nonzero)
        else:
            xOffsetDt = self.robot.rollPID.calc(0, xOffset)
            self.robot.notPitch()
            self.robot.notRotate()
            self.robot.roll(xOffsetDt)
    def action(self, imgs):
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, BLACK_MIN, BLACK_MAX)
        img_gray_line_nonzero = img_gray[2*Y//3].nonzero()
        if len(img_gray_line_nonzero[0])>4 or self.isShapeFound:
            self.isShapeFound = True
            img_gray_nonzero = img_gray.nonzero()
            self.fixRollError(img_gray, img_gray_nonzero)
            print("")
        else:
            self.robot.pitch(Y//2)
        cv2.line(img_gray, (0, 2*Y//3), (X, 2*Y//3), (0,0,255))
        cv2.line(img_gray, (0, 2*Y//3+1), (X, 2*Y//3+1), (255,255,255))
        cv2.line(img_gray, (X//2, 0), (X//2, Y), (0,0,255))
        cv2.line(img_gray, (X//2+1, 0), (X//2+1, Y), (255,255,255))
        cv2.line(img_gray, (0, Y//2), (X, Y//2), (0,0,255))
        cv2.line(img_gray, (0, Y//2+1), (X, Y//2+1), (255,255,255))
        cv2.imshow('gray', img_gray)


# Распознавание фигуры,
# если треугольник - поворот на 360 по часовой стрелке
# если круг - против часовой
class MoveOnShape():
    def __init__(self):
        self.substate = 0
        self.lastYawError = 1
    def start(self, robot):
        self.robot = robot
    def exit(self):
        self.robot.notMove()
    def action(self, imgs):
        img = imgs[0]
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, BLACK_MIN, BLACK_MAX)
#        gray = np.bitwise_not(gray)
#        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
#        thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
        # cv2.imshow('dst', thresh)
        # cv2.waitKey()
 
        cnts = cv2.findContours(img_gray.copy(), cv2.RETR_LIST,
                                cv2.CHAIN_APPROX_SIMPLE)
        #cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        cnts = cnts[1]
 
        figType = None # 1 - треугольник, 0 - круг
        squarePoints = None
        for c in cnts:
            isNext = False
            for point in c:
                # Исключаем контур, если он у края
                if (point[0][0] < 10) or (point[0][0] > (X - 10)) or\
                   (point[0][1] < 10) or (point[0][1] > (Y - 10)):
                   isNext = True
                   break
            if isNext:
                continue
            peri = cv2.arcLength(c, True)
            # Исключаем контур с маленьким периметром
            if peri < 100:
                continue
            # аппроксимируем контур
            approx = cv2.approxPolyDP(c, 0.04 * peri, True)
            # Находим центр контура
            M = cv2.moments(c)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            if len(approx) == 3:
                #print('Треугольник', end='; ')
                figType = 1
            elif len(approx) == 4:
                #print(approx, end = '; ')
                #print('Квадрат', end='; ')
                squarePoints = approx
            else:
                #print('Круг (число вершин:', len(approx), ')', end='; ')
                figType = 0
            #print('Центр:', cX, cY, end='; ')
            #print("")
            # Рисуем контур и точку в центре
            cv2.drawContours(img, [approx], -1, (0, 255, 0), 2)
            cv2.circle(img, (cX, cY), 7, (255, 20, 20), -1)
        if (type(figType) != type(None)) and (type(squarePoints) != type(None)):
            squarePoints = squarePoints[:,0]
            squarePointsSort = squarePoints[squarePoints[:,1].argsort()]
            a = (0, Y)
            b1 = ( squarePointsSort[0:2][:,0].mean(), squarePointsSort[0:2][:,1].mean() ) # down
            b2 = ( squarePointsSort[2:4][:,0].mean(), squarePointsSort[2:4][:,1].mean() ) # up
            b = (b1[0] - b2[0], Y - (b1[1] + (Y - b2[1])))
            rotateAngle = angleBetweenVectors(a, b)
            if b[0] < 0:
                rotateAngle = -rotateAngle
            print("Yaw error", rotateAngle, end='; ')
            if figType == 0:
                print("Circle", end='; ')
                if (rotateAngle < 0) and (self.lastYawError >=0):
                    print("Step", self.substate, end='; ')
                    self.substate += 1
                targetYaw = 46
            else:
                print("Triangle", end='; ')
                if (rotateAngle > 0) and (self.lastYawError <=0):
                    print("Step", self.substate, end='; ')
                    self.substate += 1
                targetYaw = -46
            self.lastYawError = rotateAngle
            if self.substate == 4:
                targetYaw = 0
                if abs(rotateAngle) < MIN_ANGLE:
                    self.robot.state = RobotState.MOVE_TO_LINE3
            rotateAngleDt = self.robot.rotatePID.calc(targetYaw, rotateAngle)
            self.robot.rotate(rotateAngleDt)
            cv2.line(img, (int(b1[0]), int(b1[1])), (int(b2[0]), int(b2[1])), (0,0,255))
            print("")
        cv2.imshow('grayFront', img_gray)
#        cv2.imshow('thresh', thresh)
        cv2.imshow('gray', img)

@enum.unique
class RobotState(enum.Enum):
    START = StartState()
    END = EndState()
    UNKNOWN = 0

    MOVE_TO_GATE = MoveToGate()
    MOVE_NEAR_GATE = MoveNearGate()
    MOVE_NEAR_GATE2 = MoveNearGate2()

    MOVE_ON_LINE1 = MoveOnLine1()
    MOVE_TO_LINE2 = MoveToLine23(2)
    MOVE_ON_LINE2 = MoveOnLine23(2)
    MOVE_TO_LINE3 = MoveToLine23(3)
    MOVE_ON_LINE3 = MoveOnLine23(3)

    MOVE_TO_SHAPE = MoveToShape()
    MOVE_ON_SHAPE = MoveOnShape()

class Robot():
    def __init__(self):
        self.state = RobotState.START
        self.state.value.start(self, RobotState.MOVE_TO_GATE)
        self.rotatePID = PID(0.6, 0, 0.2)#1, 0.2)
        self.rollPID = PID(3, 0, 0.04)
        self.pitchPID = PID(3, 0, 0.04)
#        self.isNextLineFound = False

#     def moveForward(self, x):
#         dx = x*math.cos(toRad(self.angle-90))
#         dy = x*math.sin(toRad(self.angle+90))
#         self.x += dx
#         self.y -= dy
# #        a1 = 2000
# #        port.write(b'p' + a1.to_bytes(2, 'big'))
# #        time.sleep(FORWARD_SLEEP_TIME)
# #        a2 = 1500
# #        port.write(b'p' + a2.to_bytes(2, 'big'))
# #        time.sleep(FORWARD_SLEEP_TIME_END)
 
    # def moveBackward(self, x):
    #     dx = x*math.cos(toRad(self.angle-90))
    #     dy = x*math.sin(toRad(self.angle+90))
    #     self.x -= dx
    #     self.y += dy

    def throttle(self, value):
        port.write(b't' + int(value).to_bytes(2, 'big'))

    def notMove(self):
        self.notPitch()
        self.notRoll()
        self.notRotate()
        self.throttle(1500)

    def notPitch(self):
        a = 1500
        port.write(b'p' + a.to_bytes(2, 'big'))

    def pitch(self, angle):
        #[-Y/2..Y/2] -> [1000..2000]
        angle += Y//2
        angle *= MAX_ANGLE_COEFF * (1000/Y) # шаг
        angle += 1500 - 1000*MAX_ANGLE_COEFF/2
        angle = np.clip(angle, 1000, 2000)
        port.write(b'p' + int(angle).to_bytes(2, 'big'))

    def notRoll(self):
        a = 1500
        port.write(b'r' + a.to_bytes(2, 'big'))

    def roll(self, angle):
        #[-X/2..X/2] -> [1000..2000]
        #print(angle)
        angle += X//2
        angle *= MAX_ANGLE_COEFF * (1000/X) # шаг
        angle += 1500 - 1000*MAX_ANGLE_COEFF/2
        angle = np.clip(angle, 1000, 2000)
        #if(angle > 1499 and angle < 1501):
        #    angle = 1500
        port.write(b'r' + int(angle).to_bytes(2, 'big'))

    def notRotate(self):
        a = 1500
        port.write(b'y' + a.to_bytes(2, 'big'))

    def rotate(self, angle):
        #[-90..90] -> [1000..2000]
        #print(angle)
        angle += 90
        angle *= 5.555 # шаг (1000/180)
        angle += 1000
        angle = np.clip(angle, 1000, 2000)
        #if(angle > 1499 and angle < 1501):
        #    angle = 1500
        port.write(b'y' + int(angle).to_bytes(2, 'big'))
 
    # def rotate1(self, angle):
    #     self.angle += angle
    #     if self.angle < 0 or self.angle > 359:
    #         self.angle = self.angle % 360
    #     if(angle < 0):
    #         #sock.sendall(b"3")
    #         a1 = 1000
    #         port.write(b'y' + a1.to_bytes(2, 'big'))
    #     else:
    #         #sock.sendall(b"4")
    #         a1 = 2000
    #         port.write(b'y' + a1.to_bytes(2, 'big'))
    #     time.sleep(ROTATE_SLEEP_TIME)
    #     #sock.sendall(b"0")
    #     a2 = 1500
    #     port.write(b'y' + a2.to_bytes(2, 'big'))
    #     time.sleep(ROTATE_SLEEP_TIME_END)
 
    def go(self, imgs):
        cv2.imshow('orig', imgs[0])
        cv2.imshow('origFront', imgs[1])
        if IS_CAM_TEST:
            return
#        t1 = datetime.datetime.now()
        lastState = self.state
        self.state.value.action(imgs)
        if self.state != lastState:
            print('[', lastState.name, '->', self.state.name, ']')
            lastState.value.exit()
            self.state.value.start(self)
            #self.notMove()
            self.pitchPID.reset()
            self.rollPID.reset()
            self.rotatePID.reset()
#        t3 = datetime.datetime.now() - t1
#        print("  took1:", t3.seconds, t3.microseconds)
 
    def moveOnLine(self, img):
        img_blur = cv2.medianBlur(img, 21)
        img_hsv = cv2.cvtColor(img_blur, cv2.COLOR_BGR2HSV)
        img_gray = cv2.inRange(img_hsv, ORANGE_MIN, ORANGE_MAX)
 
        line0_posY = Y//3
        line1_posY = Y//2
        line2_posY = 2*Y//3
        img_gray_nonzero0 = img_gray[line0_posY].nonzero()
        img_gray_nonzero1 = img_gray[line1_posY].nonzero()
        img_gray_nonzero2 = img_gray[line2_posY].nonzero()
        if (len(img_gray_nonzero0[0]) > STRIP_SIZE):
            if (len(img_gray_nonzero1[0]) > STRIP_SIZE) and (len(img_gray_nonzero2[0]) > STRIP_SIZE):
                p = int(img_gray_nonzero1[0].mean())
                p2 = int(img_gray_nonzero2[0].mean())
                cv2.circle(img_gray, (p, line1_posY), 5, (0,0,255))
                cv2.circle(img_gray, (p2, line2_posY), 5, (0,0,255))
                cv2.circle(img_gray, (p, line1_posY), 8, (255,255,255))
                cv2.circle(img_gray, (p2, line2_posY), 8, (255,255,255))
                a = (0, 1)
                b = (p - p2, line2_posY - line1_posY)
                #cs = (a[0]*b[0] + a[1]*b[1]) / (1 * math.sqrt(b[0]*b[0] + b[1]*b[1]))

                c = np.dot(a,b)/np.linalg.norm(a)/np.linalg.norm(b) # -> cosine of the angle
                rotateAngleRad = np.arccos(np.clip(c, -1, 1)) # angle
                rotateAngle = toDeg(rotateAngleRad)
                if b[0] < 0:
                    rotateAngle = -rotateAngle

                print("Yaw error", rotateAngle, end='; ')
                if abs(rotateAngle) < MIN_ANGLE:
                    self.notRotate()
                    p12 = (p+p2) // 2
                    line12_posY = (line1_posY+line2_posY) // 2
                    cv2.circle(img_gray, (p12, line12_posY), 5, (0,0,255))
                    cv2.circle(img_gray, (p12, line12_posY), 8, (255,255,255))
                    xOffset = X//2 - p12
                    print("Roll error", xOffset, end='; ')
                    if abs(xOffset) < MIN_ROLL_ERROR:
                        print("Go", end='; ')
                        self.notRoll()
                        self.pitch(Y//2)
                        #print("  Move forward")
                        #self.moveForward(20)
                    else:
                        xOffsetDt = self.rollPID.calc(0, xOffset)
                        self.notPitch()
                        self.roll(xOffsetDt)
                else:
                    rotateAngleDt = self.rotatePID.calc(0, rotateAngle)
                    self.notRoll()
                    self.notPitch()
                    self.rotate(rotateAngleDt)
        else:
            # робот вышел за линию
            print("Out of line", end='; ')
            self.notRoll()
            self.notRotate()
            self.pitch(Y//2)
            time.sleep(2)
            self.state = RobotState.OUT_OF_LINE
        print("")
        cv2.imshow('gray', img_gray)

class Camera():
    def __init__(self):
        if not IS_SIMULATION:
            if not cap.isOpened():
                print('Error: cap is not opened')
                exit()
            if not capFront.isOpened():
                print('Error: capFront is not opened')
                exit()
        self.oldimg = None
        #ret, self.img = cap.read()
        self.isNewCapRead = False

    def recvImages(self, clientSocket):
        buf = b''
        while True:
            buf += clientSocket.recv(2*X*Y*3 - len(buf))
            if len(buf) >= 2*X*Y*3:
                break
        nparr = np.frombuffer(buf[0:X*Y*3], np.uint8).reshape((Y, X, 3))
        img1 = cv2.flip(nparr, 0)
        nparr = np.frombuffer(buf[X*Y*3:], np.uint8).reshape((Y, X, 3))
        img2 = cv2.flip(nparr, 0)
        return (cv2.cvtColor(img1, cv2.COLOR_RGB2BGR), cv2.cvtColor(img2, cv2.COLOR_RGB2BGR))

    def readFromCameraLoop(self):
        while True:
#            t1 = datetime.datetime.now()
            ret, self.img = cap.read()
            retFront, self.imgFront = capFront.read()
            self.isNewCapRead = True
            if ret == False or retFront == False:
                print('cap.read error')
                exit()
#            t3 = datetime.datetime.now() - t1
#            print("  took:", t3.seconds, t3.microseconds)

    def readFromSocket(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Создание сокета
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #Разрешение переиспользования порта
        sock.bind(('0.0.0.0', 21000)) #Привязка сокета к интерфейсу(0.0.0.0 - все интерфейсы) и порту
        sock.listen(1) #Кол-во клиентов в очереди
        print("Waiting for client")
        clientSocket, clientAddr = sock.accept() #Ожидание подключения клиента
        print("Connected:", clientAddr)
        while True:
            #t1 = datetime.datetime.now()
            self.img, self.imgFront = self.recvImages(clientSocket)
            #self.imgFront = self.recvImage(clientSocket)
            self.isNewCapRead = True
            #t3 = datetime.datetime.now() - t1
            #print("  took:", t3.seconds, t3.microseconds)

    def loop(self):
        if IS_SIMULATION:
            self.readFromSocket()
        else:
            self.readFromCameraLoop()

    # возвращаемое значение:
    # картинка с нижней камеры
    # картинка с передней камеры
    def getImage(self):
        while self.isNewCapRead == False:
            if type(self.oldimg) != type(None):
                cv2.imshow('orig', self.oldimg)
                cv2.imshow('origFront', self.oldimgFront)
                cv2.waitKey(1)
                #time.sleep(2)
            #pass
        self.oldimg = self.img
        self.oldimgFront = self.imgFront
        self.isNewCapRead = False
        return self.img, self.imgFront
#    def getImageNoThread(self):
#        ret, img = cap.read()
#        ret, imgFront = capFront.read()
#        return img, imgFront

def init():
    if IS_SIMULATION:
        global port
        port = TCP_Client()
    else:
        global cap
        global capFront
        #pyMultiWii serial protocol
        #port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=3.0)
        #port = PortFake()

        cap = cv2.VideoCapture(0)
        capFront = cv2.VideoCapture(1)
        #cap = cv2.VideoCapture("gst-launch-1.0 -v tcpclientsrc host=192.168.1.13 port=5000  ! gdpdepay !  rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sync=false ")
        #cap = cv2.VideoCapture("tcpclientsrc host=10.0.0.1 port=5000 ! gdpdepay ! rtph264depay ! video/x-h264, width=1280, height=720, format=YUY2, framerate=49/1 ! ffdec_h264 ! autoconvert ! appsink sync=false")
        #cap = cv2.VideoCapture("gst-launch tcpclientsrc host=10.0.0.1 port=5000 ! gdpdepay ! rtph264depay ! video/x-h264, width=1280, height=720, format=YUY2, framerate=49/1 ! ffdec_h264 ! appsink sync=false")

        cap.set(cv2.CAP_PROP_FRAME_WIDTH, X)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, Y)

        capFront.set(cv2.CAP_PROP_FRAME_WIDTH, X)
        capFront.set(cv2.CAP_PROP_FRAME_HEIGHT, Y)
        #cap.set(cv2.CV_CAP_PROP_FPS, 15)

def main():
    init()
    camera = Camera()
    cameraThread = threading.Thread(target=camera.loop)
    cameraThread.daemon = True
    cameraThread.start()

    robot = Robot()

    while True:
        imgs = camera.getImage()
        #cv2.imshow('dst', img)
        #cv2.waitKey(0)
        robot.go((imgs[0].copy(), imgs[1].copy()))
        if cv2.waitKey(1) & 0xff == 27:
            cv2.destroyAllWindows()
            break
        #time.sleep(2)
    #sock.shutdown(socket.SHUT_RDWR)
    #sock.close()

if __name__ == '__main__':
    main()